# Todolist for f2e interview

## 簡介

這是一個 TodoList App 的專案，目前什麼都沒有，希望依照設計稿實做。

## 設計稿

設計稿都存在 /mockups 裡面

我只設了一個 break point: 768px

width < 768px 請參照 f2e-mobile

width >= 768px 請參照 f2e-tablet

### Mobile

行動裝置版說明如下
* 每個 grid 代表 8.33% x 16px
* 左右各留4px padding
* 
### Tablet

平板/桌面版說明如下
* 每個 grid 代表 62px x 16px
* 

## 其他說明

我希望他是 Mobile-first App，所以請先以 mobile 設計稿優先開發。

設計稿另外提供 sketcy / zepplin 版本，建議可以註冊一個帳號使用，連結如下：http://zpl.io/1VDvgg

## 評分標準

下面列出一些項目，不必全部做完，也不必追求高分

但為求方便我簡單訂出一個參考標準：

60分是一個及格標準，達到此分數就有進入下一關面試的門票
100分是一個滿分標準，不需要追求比100分還高的分數

以下是評分項目：
* 基本分區
    * 架構出 HTML
        * 滿分20分
            * DOM Tree 合理10分
            * Tag 合理10分
    * 如同設計稿的切出版型
        * 滿分20分
            * 長得差不多給15分，太多地方不一樣給10分
            * CSS 規劃妥當給5分
    * 新增Task功能
        * 滿分10分
    * 刪除Task功能
        * 滿分10分
    * 做出兩種版型
        * 滿分10分
            * 跟設計稿一樣給5分
            * CSS重用率高給5分
* 進階區
    * 動態產生今天日期以及時間 並寫入最前列
        * 滿分10分
    * 當 "With done" 打勾時，隱藏所有已經打勾的 Task
        * 滿分10分
    * 隱藏已經打勾的 Task 時，做出向左移出的效果
        * 滿分10分
    * 連結後台的測資產生 HTML
        * 滿分10分
    * 連結後台測資，儲存表單內容（詳情見下方）
        * 滿分30分
            * 新增10分
            * 修改10分
            * 刪除10分
            
## Api Doc

### Task

#### Read 取得清單
* Endpoint
    * `{account}.todolist.simpleinfo.tw/task`
* Method
    * `get`
* Parameters
    * none
* Example
    * `jQuery.get('http://fzhan.todolist.simpleinfo.tw/task')`

#### Create 新增任務
* Endpoint
    * `{account}.todolist.simpleinfo.tw/task`
* Method
    * `post`
* Parameters
    * content
        * 字串，長度大於等於
        * 必填
* Example
    * `jQuery.post('http://fzhan.todolist.simpleinfo.tw/task', {content: "My new task"})`

#### Update 修改任務
* Endpoint
    * `{account}.todolist.simpleinfo.tw/task/{id}`
* Method
    * `post`
* Parameters
    * content
        * content
            * 字串，長度大於等於
        * done
            * 布林，僅接受 1, 0, true, false 
* Example
    * `jQuery.post('http://fzhan.todolist.simpleinfo.tw/task/1', {content: "My new task(writing down doc)", done: false})`

#### Delete 刪除任務
* Endpoint
    * `{account}.todolist.simpleinfo.tw/task/{id}/delete`
* Method
    * `post`
* Parameters
    * none
* Example
    * `jQuery.post('http://fzhan.todolist.simpleinfo.tw/task/1/delete')`



## 上傳注意事項
1. 請用自己的 id 開一個 branch，例如 fzhan
2. 請先設定 local git config，設定方法如下

    `git config user.name fzhan`
    
    `git config user.email fzhan@simpleinfo.cc`
